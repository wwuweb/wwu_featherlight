<?php

namespace Drupal\wwu_featherlight\Plugin\Filter;

use Drupal\Component\Utility\Html;
use Drupal\filter\Plugin\FilterBase;
use Drupal\filter\FilterProcessResult;

/**
 * Adds the Featherlight library when the data-featherlight attribute is
 * present.
 *
 * @Filter(
 *   id = "filter_wwu_featherlight",
 *   title = @Translation("Featherlight"),
 *   description = @Translation("Load Featherlight library when data-featherlight is present"),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_MARKUP_LANGUAGE
 * )
 */
class FilterFeatherlight extends FilterBase {

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    $result = new FilterProcessResult($text);
    $html = Html::load($text);
    $xpath = new \DOMXPath($html);
    $elements = $xpath->query("//*[@data-featherlight]");

    if (count($elements)) {
      $result->setAttachments([
        'library' => ['ashlar/featherlight'],
      ]);
    }

    return $result;
  }

}
